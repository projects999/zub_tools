//todo: Add this class in main project
import 'package:flutter/material.dart';
import 'package:seed_for_me_app/tools/resources/strings/multi_lang.dart';
import 'package:seed_for_me_app/tools/utilities/utils.dart';
import 'package:seed_for_me_app/tools/widgets/support/edit_text_controller.dart';

typedef OnValidateCallback = FocusNode Function();
typedef OnSuccessCallback = void Function();
typedef OnFailedCallback = void Function(FocusNode focusNode);

mixin ValidationProvider {
  static void getInstance({
    OnValidateCallback onValidate,
    OnSuccessCallback onSuccess,
    OnFailedCallback onFailed,
  }) {
    FocusNode focus = onValidate();
    if (focus != null) {
      onFailed(focus);
    } else {
      onSuccess();
    }
  }

// static bool isValidEmail(String email) {
//   return RegExp(
//           r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
//       .hasMatch(email);
// }
}

class ValidateText {
  bool isAllowEmpty;
  bool isEmail;
  bool isComplexPassword;
  ValidateBetween betweenLength;
  bool hasCapitalLater;
  bool hasSmallLater;
  bool hasNumber;
  bool hasSpecialChar;
  //Error messages
  String isAllowEmptyMsg;
  String isComplexPasswordMsg;
  String isEmailMsg;
  String betweenLengthMsg;
  String hasCapitalLaterMsg;
  String hasSmallLaterMsg;
  String hasNumberMsg;
  String hasSpecialCharMsg;

  ValidateText({
    this.isAllowEmpty,
    this.isEmail,
    this.isComplexPassword,
    this.betweenLength,
    this.hasCapitalLater,
    this.hasSmallLater,
    this.hasNumber,
    this.hasSpecialChar,
    //Error messages
    this.isEmailMsg,
    this.isComplexPasswordMsg,
    this.betweenLengthMsg,
    this.isAllowEmptyMsg,
    this.hasCapitalLaterMsg,
    this.hasSmallLaterMsg,
    this.hasNumberMsg,
    this.hasSpecialCharMsg,
  });

  String validate(String text) {
    String errorMsg;

    if (isAllowEmpty != null && isAllowEmpty == false) {
      errorMsg = _validateAllowEmpty(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (isEmail != null && isEmail == true) {
      errorMsg = _validateEmail(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (isComplexPassword != null && isComplexPassword == true) {
      errorMsg = _validateIsComplexPassword(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (betweenLength != null) {
      errorMsg = _validateBetween(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (hasCapitalLater != null && hasCapitalLater == true) {
      errorMsg = _validateHasCapitalLetter(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (hasSmallLater != null && hasSmallLater == true) {
      errorMsg = _validateHasSmallLetter(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (hasNumber != null && hasNumber == true) {
      errorMsg = _validateHasNumber(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    if (hasSpecialChar != null && hasSpecialChar == true) {
      errorMsg = _validateHasSpecialChar(text);
      if (errorMsg != null) {
        return errorMsg;
      }
    }

    //[errorMsg] will be null if all validations are ok.
    return errorMsg;
    // return null;
  }

  String _validateEmail(String text) {
    if (RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(text)) {
      return null;
    } else {
      return isEmailMsg ?? MultiLang.value('invalid_email');
    }
  }

  String _validateAllowEmpty(String text) {
    if (text.isNotEmpty) {
      return null;
    } else {
      return isAllowEmptyMsg ?? MultiLang.value('empty_not_allow');
    }
  }

  String _validateBetween(String text) {
    if (text.length >= betweenLength.min && text.length <= betweenLength.max) {
      return null;
    } else {
      return betweenLengthMsg ?? MultiLang.value('invalid_length');
    }
  }

  String _validateHasCapitalLetter(String text) {
    if (text.contains(RegExp(r'[A-Z]'))) {
      return null;
    } else {
      return hasCapitalLaterMsg ?? MultiLang.value('cap_letter_req');
    }
  }

  String _validateHasSmallLetter(String text) {
    if (text.contains(RegExp(r'[a-z]'))) {
      return null;
    } else {
      return hasSmallLaterMsg ?? MultiLang.value('small_letter_req');
    }
  }

  String _validateHasNumber(String text) {
    if (text.contains(RegExp(r'[0-9]'))) {
      return null;
    } else {
      return hasNumberMsg ?? MultiLang.value('num_req');
    }
  }

  String _validateHasSpecialChar(String text) {
    if (text.contains(RegExp(r'[!@#$%^&*(),.?":{}|<>]'))) {
      return null;
    } else if (text.contains('[')) {
      return null;
    } else if (text.contains(']')) {
      return null;
    } else if (text.contains(r"'")) {
      return null;
    } else {
      return hasSpecialCharMsg ?? MultiLang.value('special_char_req');
    }
  }

  String _validateIsComplexPassword(String text) {
    String _hasCapLetter = _validateHasCapitalLetter(text);
    String _hasSmallLetter = _validateHasSmallLetter(text);
    String _hasNumber = _validateHasNumber(text);
    String _hasSpecialChar = _validateHasSpecialChar(text);

    if (_hasCapLetter == null &&
        _hasSmallLetter == null &&
        _hasNumber == null &&
        _hasSpecialChar == null) {
      return null;
    } else {
      return isComplexPasswordMsg ?? MultiLang.value('comp_pass_req');
    }
  }
}

class ValidateBetween {
  int min;
  int max;

  ValidateBetween(this.min, this.max);
}
